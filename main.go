package main

import "fmt"

func add(a, b int) int{
	return a + b
}
func sub(c, d int) int {
	return c - d
}
func main() {

	fmt.Println("hello world")
	fmt.Println("Always,hope for the best!!")
	num1 := 10
	num2 := 20
	sum :=add(num1, num2)
	fmt.Printf("sum of %d and %d is: %d\n", num1, num2, sum)
	fmt.Println("Book of Life..!")

	c := 20
	d := 10
	res := sub(c,d)
	fmt.Printf("sub of %d and %d is: %d\n", c, d, res)
	fmt.Println("Faith can move mountains")

}
